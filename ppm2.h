#ifndef PPM2_H
#define PPM2_H

#include <QWidget>

namespace Ui {
    class PPM2;
}

class PPM2 : public QWidget
{
    Q_OBJECT

public:
    explicit PPM2(QWidget *parent = 0);
    ~PPM2();


private slots:
    void on_pH_SpinBox_editingFinished();

    // the following function will calculate PPM
    // for given pH,pKa and molecular SO2
    void calculate_PPM();
    // the following function will calculate
    // the grams of K2S2O5 needed to bring
    // a wine volume to a PPM SO2 desired
    void calculate_KMS_grams();

    void on_pKa_SpinBox_editingFinished();

    void on_molso2_SpinBox_editingFinished();

    void on_DesiredPPM_lineEdit_editingFinished();

    void on_Gallons_lineEdit_returnPressed();

    void on_Liters_lineEdit_returnPressed();

    void on_areYouSureButton_clicked();

    void on_LabPPM_lineEdit_returnPressed();

private:
    Ui::PPM2 *ui;
};

#endif // PPM2_H
