#include "ppm2.h"
#include "ui_ppm2.h"
#include <QtGui>
#include <QtCore>
#include <QString>
#include <QLocale>
#include <QMessageBox>
#include <QDate>


PPM2::PPM2(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PPM2)
{
    ui->setupUi(this);
}

PPM2::~PPM2()
{
    delete ui;
}


void PPM2::calculate_PPM()
{
    double pH;
    double pKa;
    double molso2;
    double ppm;
    double labppm;
    double needppm;



    QString txt_pH;
    QString txt_pKa;
    QString txt_molso2;
    QString txt_ppm;
    QString txt_labPPM;
    QString txt_needPPM;
    QLocale conv;


        txt_pH = ui->pH_SpinBox->text();
        pH = txt_pH.toDouble();


        txt_pKa = ui->pKa_SpinBox->text();
        pKa = txt_pKa.toDouble();

        txt_molso2 = ui->molso2_SpinBox->text();
        molso2 = txt_molso2.toDouble();

        txt_labPPM = ui->LabPPM_lineEdit->text();
        labppm = txt_labPPM.toDouble();

        ppm = molso2*pow(10,pH-pKa)+1;

        needppm = ppm-labppm;

        txt_ppm=conv.toString(ppm);
        txt_needPPM = conv.toString(needppm);

    ui->CalcPPM_lineEdit->clear();
    ui->CalcPPM_lineEdit->insert(txt_ppm);

    ui->needPPM_lineEdit->clear();
    ui->needPPM_lineEdit->insert(txt_needPPM);

    ui->DesiredPPM_lineEdit->clear();
    ui->DesiredPPM_lineEdit->insert(txt_needPPM);

    calculate_KMS_grams();
}

 void PPM2::calculate_KMS_grams()
 {
     double dppm;  // desired PPM
     double gallons;
     double liters;
     double grams_total;

     QString txt_gallons;
     QString txt_liters;
     QString txt_dppm;
     QString txt_grams_total;
     QLocale convert;

     if(ui->Gallons_Rbutton->isChecked() == true)
     {
         liters=0;
         txt_gallons=ui->Gallons_lineEdit->text();
         gallons = txt_gallons.toDouble();
         txt_dppm.clear();
         txt_dppm=ui->DesiredPPM_lineEdit->text();
         dppm = txt_dppm.toDouble();
         liters=gallons*3.7854;
     }
     else
        {
            gallons = 0;
            txt_liters = ui->Liters_lineEdit->text();
            liters = txt_liters.toDouble();
            txt_dppm.clear();
            txt_dppm=ui->DesiredPPM_lineEdit->text();
            dppm = txt_dppm.toDouble();
            gallons = liters/3.7854;

        }
     grams_total=(liters*dppm)/(1000*0.57);
     txt_gallons.clear();
     txt_gallons=convert.toString(gallons);
     txt_liters.clear();
     txt_liters=convert.toString(liters);
     txt_grams_total.clear();
     txt_grams_total=convert.toString(grams_total);
     ui->Gallons_lineEdit->clear();
     ui->Gallons_lineEdit->insert(txt_gallons);
     ui->Liters_lineEdit->clear();
     ui->Liters_lineEdit->insert(txt_liters);
     ui->CalcGrams_lineEdit->clear();
     ui->CalcGrams_lineEdit->insert(txt_grams_total);

 }


void PPM2::on_pH_SpinBox_editingFinished()
{
    calculate_PPM();
}

void PPM2::on_pKa_SpinBox_editingFinished()
{
    calculate_PPM();
}

void PPM2::on_molso2_SpinBox_editingFinished()
{
    calculate_PPM();
}

void PPM2::on_DesiredPPM_lineEdit_editingFinished()
{
    calculate_KMS_grams();
}

void PPM2::on_Gallons_lineEdit_returnPressed()
{
    ui->Gallons_Rbutton->setChecked(true);
    calculate_KMS_grams();
}

void PPM2::on_Liters_lineEdit_returnPressed()
{
    ui->Liters_Rbutton->setChecked(true);
    calculate_KMS_grams();
}


void PPM2::on_LabPPM_lineEdit_returnPressed()
{




    calculate_PPM();




}


void PPM2::on_areYouSureButton_clicked()
{
    double grams;
    double grams_twice;

    QDate date = QDate::currentDate();
    QString today = date.toString("MMMM dd yyyy");
    QString txt_pH;
    QString txt_molso2;
    QString txt_ppm;
    QString txt_labPPM;
    QString txt_volume;
    QString txt_dppm;
    QString txt_grams;
    QString txt_dgrams;
    QString txt_grams_total;
    QString txt_wineName;
    QLocale convert;



    if(ui->Gallons_Rbutton->isChecked()==true)
    {
        txt_volume=(ui->Gallons_lineEdit->text()+" gallons");
    }

    else
    {
        txt_volume = (ui->Liters_lineEdit->text()+" liters");

    }
    txt_ppm = (ui->CalcPPM_lineEdit->text());
    txt_pH = (ui->pH_SpinBox->text());
    txt_molso2 = (ui->molso2_SpinBox->text());
    txt_dppm = (ui->DesiredPPM_lineEdit->text());
    txt_labPPM = (ui->LabPPM_lineEdit->text()+" PPM SO2");
    txt_grams_total = (ui->CalcGrams_lineEdit->text()+" grams of potassium metabisulfite");
    txt_grams = (ui->CalcGrams_lineEdit->text());
    grams = txt_grams.toDouble();
    grams_twice = 2*grams;
    txt_dgrams = convert.toString(grams_twice);
    txt_wineName =(ui->wine_name_lineEdit->text());

    QMessageBox msgBox;

    if(ui->FirstAdditionCK->isChecked()==true)
    {
        msgBox.setText("Make Sure This is Correct!!       "+today+"\n\n"
                   "You are treating "+txt_volume+" of "+txt_wineName+" wine.\n"
                   "The wine has a pH of "+txt_pH+".\n"
                   "Molecular SO2 desired is "+txt_molso2+".\n"
                   "Calculated SO2 needed is "+txt_ppm+".\n"
                   "Lab results show "+txt_labPPM+" are in the wine.\n"
                   "Parts per million SO2 addition desired is "+txt_dppm+". \n"
                   "You need "+txt_grams_total+".\n\n"
                   "First time Addition Box has been checked. Consider doubling\n"
                   "the amount of potassium metabisulfite to "+txt_dgrams+" grams to\n"
                   "compensate for SO2 binding.\n\n "
                   "Last but not least make sure your scale is accurate and\n "
                   "setup for GRAMS. The formulas in this program are setup for grams!!"

                   );
    }
    else
    {
        msgBox.setText("Make Sure This is Correct!!       "+today+"\n\n"
                   "You are treating "+txt_volume+" of "+txt_wineName+" wine.\n"
                   "The wine has a pH of "+txt_pH+".\n"
                   "Molecular SO2 desired is "+txt_molso2+".\n"
                   "Calculated SO2 needed is "+txt_ppm+".\n"
                   "Lab results show "+txt_labPPM+" are in the wine.\n"
                   "Parts per million SO2 addition desired is "+txt_dppm+".\n"
                   "You need "+txt_grams_total+".\n\n"
                   "Last but not least make sure your scale is accurate and\n "
                   "setup for GRAMS. The formulas in this program are setup for grams!!");

    }
    msgBox.exec();

}

