#-------------------------------------------------
#
# Project created by QtCreator 2011-08-12T21:24:14
#
#-------------------------------------------------

QT       += core widgets

TARGET = PPM2
TEMPLATE = app


SOURCES += main.cpp\
        ppm2.cpp

HEADERS  += ppm2.h

FORMS    += ppm2.ui
