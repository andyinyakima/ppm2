/*
 *	PPM2 has no guarantees, hopefully it will help a person make good wine
 *
 *
 *  Copyright (C) 2016  Andy Laberge (andylaberge@linux.com)
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	If you did not receive a copy (most probable) of the GNU General Public License
 *	along with this program; go see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include "ppm2.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    PPM2 w;
    w.show();

    return a.exec();
}
