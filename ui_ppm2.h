/********************************************************************************
** Form generated from reading UI file 'ppm2.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PPM2_H
#define UI_PPM2_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PPM2
{
public:
    QPushButton *areYouSureButton;
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QDoubleSpinBox *pH_SpinBox;
    QLabel *label;
    QDoubleSpinBox *pKa_SpinBox;
    QLabel *label_2;
    QDoubleSpinBox *molso2_SpinBox;
    QLineEdit *LabPPM_lineEdit;
    QLineEdit *CalcPPM_lineEdit;
    QLabel *label_7;
    QLabel *label_4;
    QLabel *label_3;
    QLineEdit *wine_name_lineEdit;
    QLabel *label_9;
    QWidget *layoutWidget1;
    QGridLayout *gridLayout_2;
    QLineEdit *Gallons_lineEdit;
    QRadioButton *Gallons_Rbutton;
    QLineEdit *Liters_lineEdit;
    QRadioButton *Liters_Rbutton;
    QLineEdit *CalcGrams_lineEdit;
    QLineEdit *DesiredPPM_lineEdit;
    QLabel *label_6;
    QLineEdit *needPPM_lineEdit;
    QLabel *label_5;
    QLabel *label_8;
    QCheckBox *FirstAdditionCK;

    void setupUi(QWidget *PPM2)
    {
        if (PPM2->objectName().isEmpty())
            PPM2->setObjectName(QStringLiteral("PPM2"));
        PPM2->setEnabled(true);
        PPM2->resize(584, 288);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(PPM2->sizePolicy().hasHeightForWidth());
        PPM2->setSizePolicy(sizePolicy);
        areYouSureButton = new QPushButton(PPM2);
        areYouSureButton->setObjectName(QStringLiteral("areYouSureButton"));
        areYouSureButton->setGeometry(QRect(230, 260, 115, 27));
        areYouSureButton->setFocusPolicy(Qt::StrongFocus);
        layoutWidget = new QWidget(PPM2);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 10, 231, 231));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        pH_SpinBox = new QDoubleSpinBox(layoutWidget);
        pH_SpinBox->setObjectName(QStringLiteral("pH_SpinBox"));
        pH_SpinBox->setFocusPolicy(Qt::StrongFocus);
        pH_SpinBox->setMinimum(2.7);
        pH_SpinBox->setMaximum(4.5);
        pH_SpinBox->setSingleStep(0.01);
        pH_SpinBox->setValue(3.5);

        gridLayout->addWidget(pH_SpinBox, 0, 0, 1, 1);

        label = new QLabel(layoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 1, 1, 1);

        pKa_SpinBox = new QDoubleSpinBox(layoutWidget);
        pKa_SpinBox->setObjectName(QStringLiteral("pKa_SpinBox"));
        pKa_SpinBox->setFocusPolicy(Qt::StrongFocus);
        pKa_SpinBox->setMinimum(1.75);
        pKa_SpinBox->setMaximum(1.82);
        pKa_SpinBox->setSingleStep(0.01);
        pKa_SpinBox->setValue(1.81);

        gridLayout->addWidget(pKa_SpinBox, 1, 0, 1, 1);

        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 1, 1, 1);

        molso2_SpinBox = new QDoubleSpinBox(layoutWidget);
        molso2_SpinBox->setObjectName(QStringLiteral("molso2_SpinBox"));
        molso2_SpinBox->setFocusPolicy(Qt::StrongFocus);
        molso2_SpinBox->setMinimum(0.5);
        molso2_SpinBox->setMaximum(2);
        molso2_SpinBox->setSingleStep(0.1);
        molso2_SpinBox->setValue(0.8);

        gridLayout->addWidget(molso2_SpinBox, 2, 0, 1, 1);

        LabPPM_lineEdit = new QLineEdit(layoutWidget);
        LabPPM_lineEdit->setObjectName(QStringLiteral("LabPPM_lineEdit"));

        gridLayout->addWidget(LabPPM_lineEdit, 4, 0, 1, 1);

        CalcPPM_lineEdit = new QLineEdit(layoutWidget);
        CalcPPM_lineEdit->setObjectName(QStringLiteral("CalcPPM_lineEdit"));
        CalcPPM_lineEdit->setFocusPolicy(Qt::StrongFocus);
        CalcPPM_lineEdit->setReadOnly(true);

        gridLayout->addWidget(CalcPPM_lineEdit, 3, 0, 1, 1);

        label_7 = new QLabel(layoutWidget);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout->addWidget(label_7, 4, 1, 1, 1);

        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 3, 1, 1, 1);

        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 2, 1, 1, 1);

        wine_name_lineEdit = new QLineEdit(layoutWidget);
        wine_name_lineEdit->setObjectName(QStringLiteral("wine_name_lineEdit"));

        gridLayout->addWidget(wine_name_lineEdit, 5, 0, 1, 1);

        label_9 = new QLabel(layoutWidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout->addWidget(label_9, 5, 1, 1, 1);

        layoutWidget1 = new QWidget(PPM2);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(330, 10, 244, 231));
        gridLayout_2 = new QGridLayout(layoutWidget1);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        Gallons_lineEdit = new QLineEdit(layoutWidget1);
        Gallons_lineEdit->setObjectName(QStringLiteral("Gallons_lineEdit"));
        Gallons_lineEdit->setFocusPolicy(Qt::StrongFocus);

        gridLayout_2->addWidget(Gallons_lineEdit, 3, 0, 1, 1);

        Gallons_Rbutton = new QRadioButton(layoutWidget1);
        Gallons_Rbutton->setObjectName(QStringLiteral("Gallons_Rbutton"));
        Gallons_Rbutton->setFocusPolicy(Qt::NoFocus);
        Gallons_Rbutton->setChecked(true);

        gridLayout_2->addWidget(Gallons_Rbutton, 3, 1, 1, 1);

        Liters_lineEdit = new QLineEdit(layoutWidget1);
        Liters_lineEdit->setObjectName(QStringLiteral("Liters_lineEdit"));
        Liters_lineEdit->setFocusPolicy(Qt::StrongFocus);

        gridLayout_2->addWidget(Liters_lineEdit, 4, 0, 1, 1);

        Liters_Rbutton = new QRadioButton(layoutWidget1);
        Liters_Rbutton->setObjectName(QStringLiteral("Liters_Rbutton"));
        Liters_Rbutton->setFocusPolicy(Qt::NoFocus);

        gridLayout_2->addWidget(Liters_Rbutton, 4, 1, 1, 1);

        CalcGrams_lineEdit = new QLineEdit(layoutWidget1);
        CalcGrams_lineEdit->setObjectName(QStringLiteral("CalcGrams_lineEdit"));
        CalcGrams_lineEdit->setFocusPolicy(Qt::StrongFocus);
        CalcGrams_lineEdit->setReadOnly(true);

        gridLayout_2->addWidget(CalcGrams_lineEdit, 5, 0, 1, 1);

        DesiredPPM_lineEdit = new QLineEdit(layoutWidget1);
        DesiredPPM_lineEdit->setObjectName(QStringLiteral("DesiredPPM_lineEdit"));
        DesiredPPM_lineEdit->setFocusPolicy(Qt::StrongFocus);

        gridLayout_2->addWidget(DesiredPPM_lineEdit, 2, 0, 1, 1);

        label_6 = new QLabel(layoutWidget1);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_2->addWidget(label_6, 5, 1, 1, 1);

        needPPM_lineEdit = new QLineEdit(layoutWidget1);
        needPPM_lineEdit->setObjectName(QStringLiteral("needPPM_lineEdit"));
        needPPM_lineEdit->setReadOnly(true);

        gridLayout_2->addWidget(needPPM_lineEdit, 1, 0, 1, 1);

        label_5 = new QLabel(layoutWidget1);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_2->addWidget(label_5, 2, 1, 1, 1);

        label_8 = new QLabel(layoutWidget1);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_2->addWidget(label_8, 1, 1, 1, 1);

        FirstAdditionCK = new QCheckBox(PPM2);
        FirstAdditionCK->setObjectName(QStringLiteral("FirstAdditionCK"));
        FirstAdditionCK->setGeometry(QRect(30, 250, 181, 41));
        FirstAdditionCK->setChecked(true);
        QWidget::setTabOrder(pH_SpinBox, pKa_SpinBox);
        QWidget::setTabOrder(pKa_SpinBox, molso2_SpinBox);
        QWidget::setTabOrder(molso2_SpinBox, CalcPPM_lineEdit);
        QWidget::setTabOrder(CalcPPM_lineEdit, LabPPM_lineEdit);
        QWidget::setTabOrder(LabPPM_lineEdit, needPPM_lineEdit);
        QWidget::setTabOrder(needPPM_lineEdit, DesiredPPM_lineEdit);
        QWidget::setTabOrder(DesiredPPM_lineEdit, Gallons_lineEdit);
        QWidget::setTabOrder(Gallons_lineEdit, Liters_lineEdit);
        QWidget::setTabOrder(Liters_lineEdit, CalcGrams_lineEdit);
        QWidget::setTabOrder(CalcGrams_lineEdit, areYouSureButton);

        retranslateUi(PPM2);

        QMetaObject::connectSlotsByName(PPM2);
    } // setupUi

    void retranslateUi(QWidget *PPM2)
    {
        PPM2->setWindowTitle(QApplication::translate("PPM2", "Parts Per Million", 0));
        areYouSureButton->setText(QApplication::translate("PPM2", "Are you sure??", 0));
#ifndef QT_NO_TOOLTIP
        pH_SpinBox->setToolTip(QApplication::translate("PPM2", "pH is usually between \n"
"2.9 and 3.9", 0));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("PPM2", "pH", 0));
#ifndef QT_NO_TOOLTIP
        pKa_SpinBox->setToolTip(QApplication::translate("PPM2", "pKa falls between\n"
"1.77 to 1.82", 0));
#endif // QT_NO_TOOLTIP
        label_2->setText(QApplication::translate("PPM2", "pKa", 0));
#ifndef QT_NO_TOOLTIP
        molso2_SpinBox->setToolTip(QApplication::translate("PPM2", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Molecular SO<span style=\" vertical-align:sub;\">2 </span>is usually 0.8 for off-dry whites</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">and 0.5 for dry red wines.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        LabPPM_lineEdit->setToolTip(QApplication::translate("PPM2", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">PPM SO<span style=\" vertical-align:sub;\">2</span> measured in the Lab analysis of the wine sample.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        label_7->setToolTip(QApplication::translate("PPM2", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">PPM SO<span style=\" vertical-align:sub;\">2</span> measured in the Lab analysis of the wine sample.</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        label_7->setText(QApplication::translate("PPM2", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Measured</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Lab PPM SO<span style=\" vertical-align:sub;\">2</span></p></body></html>", 0));
        label_4->setText(QApplication::translate("PPM2", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Calculated</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">PPM SO<span style=\" vertical-align:sub;\">2</span></p></body></html>", 0));
        label_3->setText(QApplication::translate("PPM2", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Molecular </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">SO<span style=\" vertical-align:sub;\">2</span></p></body></html>", 0));
        wine_name_lineEdit->setText(QApplication::translate("PPM2", "No Count", 0));
        label_9->setText(QApplication::translate("PPM2", "<html><head/><body><p>Wine Name</p></body></html>", 0));
#ifndef QT_NO_TOOLTIP
        Gallons_lineEdit->setToolTip(QApplication::translate("PPM2", "Press 'Enter' (return) for \n"
"Gallons to be accepted.", 0));
#endif // QT_NO_TOOLTIP
        Gallons_Rbutton->setText(QApplication::translate("PPM2", "Gallons", 0));
#ifndef QT_NO_TOOLTIP
        Liters_lineEdit->setToolTip(QApplication::translate("PPM2", "Press 'Enter'(return) for \n"
"Liters to be accepted!", 0));
#endif // QT_NO_TOOLTIP
        Liters_Rbutton->setText(QApplication::translate("PPM2", "Liters", 0));
#ifndef QT_NO_TOOLTIP
        DesiredPPM_lineEdit->setToolTip(QApplication::translate("PPM2", "<html><head/><body><p>If this the first application of SO<span style=\" vertical-align:sub;\">2</span> you may want to double the needed because the SO<span style=\" vertical-align:sub;\">2</span> will bind with elements in the wine!</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        label_6->setText(QApplication::translate("PPM2", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Grams K<span style=\" vertical-align:sub;\">2</span>S<span style=\" vertical-align:sub;\">2</span>O<span style=\" vertical-align:sub;\">5</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">needed</p></body></html>", 0));
#ifndef QT_NO_TOOLTIP
        needPPM_lineEdit->setToolTip(QApplication::translate("PPM2", "<html><head/><body><p>This is the PPM of SO<span style=\" vertical-align:sub;\">2</span> that is needed to provide the Desired PPM SO<span style=\" vertical-align:sub;\">2. </span>This will be used to calculate the grams of K<span style=\" vertical-align:sub;\">2</span>S<span style=\" vertical-align:sub;\">2</span>O<span style=\" vertical-align:sub;\">5 </span>that's needed to achieve the PPM SO<span style=\" vertical-align:sub;\">2</span> desired. If this the first application of SO<span style=\" vertical-align:sub;\">2</span> you may want to double the needed because the SO<span style=\" vertical-align:sub;\">2</span> will bind with elements in the wine!</p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        label_5->setText(QApplication::translate("PPM2", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">PPM SO<span style=\" vertical-align:sub;\">2</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Desired</p></body></html>", 0));
        label_8->setText(QApplication::translate("PPM2", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Needed </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">PPM SO<span style=\" vertical-align:sub;\">2</span></p></body></html>", 0));
        FirstAdditionCK->setText(QApplication::translate("PPM2", "First SO2 Addition after\n"
" primary fermentation", 0));
    } // retranslateUi

};

namespace Ui {
    class PPM2: public Ui_PPM2 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PPM2_H
